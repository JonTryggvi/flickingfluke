movApp.controller('movCtrl', function($scope, apiService, $timeout, $sce, allTrailers) {
// set upphaf value fyrir slidera svo þeir skili ekki undefined þegar first er ýtt á flick me a new fluke
	$scope.modelValue=0;
	$scope.highValue=10;
	$scope.modelValueYear=1890;
	$scope.highValueYear=2016;

// year of production slider
   $scope.slider_translate_year = {
        minValue: 1890,
        maxValue: 2016,
        options: {
        	draggableRange: true,
        	noSwitching: true,
        	id: null,
        	onChange: function(sliderId, modelValue, highValue){
        		$scope.modelValueYear = modelValue;
        		$scope.highValueYear = highValue;
        	},
            ceil: 2016,
            floor: 1890,
            translate: function (value) {
                return value;
            }
        }
    };

   
// rating slider
    $scope.slider_translate = {
        minValue: 0,
        maxValue: 10,
        options: {
        	enforceStep: false,
        	draggableRange: true,
        	noSwitching: true,
        	id: null,
        	onChange: function(sliderId, modelValue, highValue){
        		$scope.modelValue = modelValue;
        		$scope.highValue = highValue;
        	},
            ceil: 10,
            floor: 0,
            translate: function (value) {
                return value;
            }
        }
    };

        

// hér er smell til að komast á the find síðuna
	$scope.hide = false;
	$scope.show = true;
	$scope.rdzr_btn = function(){
		$scope.hide = true;
		$scope.show = false;
		getMovies();
		$scope.navShow();
	};
// takki til að fara aftur á randomizersíðu
	$scope.logo_btn = function(){
		$scope.btn();
	};
// hér er hægt að toggla upp og niður filter panel
	$scope.filterCanvas = true;
	$scope.filter = function() {
	    $scope.filterCanvas = !$scope.filterCanvas;
		    $timeout(function () {
	            $scope.$broadcast('rzSliderForceRender');
	        });
	  };	
// litil function til að fela navið svo að það birtist ekki of snemma í ferlinu.
	$scope.navHide = true;
	$scope.navShow = function() {
		$scope.navHide = false;
	};
// facebook loggin functions

	$scope.statusSiText = false;
	$scope.share = function(){
		$scope.statusSiText = true;
	// This is called with the results from from FB.getLoginStatus().
		FB.ui({
			method: 'share',
			mobile_iframe: true,
			href: 'http://flickinfluke.com/'
			}, 
			function(response){
			  // Debug response (optional)
		});
	};

	$scope.genres = [
		     {
	      "id": 0,
	      "label": "All Genres",
	      "value": "All Genres"
	    },
	    {
	      "id": 28,
	      "label": "Action",
	      "value": "Action"
	    },
	    {
	      "id": 12,
	      "label": "Adventure",
	      "value": "Adventure"
	    },
	    {
	      "id": 16,
	      "label": "Animation",
	      "value": "Animation"
	    },
	    {
	      "id": 35,
	      "label": "Comedy",
	      "value": "Comedy"
	    },
	    {
	      "id": 80,
	      "label": "Crime",
	      "value": "Crime"
	    },
	    {
	      "id": 99,
	      "label": "Documentary",
	      "value": "Documentary"
	    },
	    {
	      "id": 18,
	      "label": "Drama",
	      "value": "Drama"
	    },
	    {
	      "id": 10751,
	      "label": "Family",
	      "value": "Family"
	    },
	    {
	      "id": 14,
	      "label": "Fantasy",
	      "value": "Fantasy"
	    },
	    {
	      "id": 10769,
	      "label": "Foreign",
	      "value": "Foreign"
	    },
	    {
	      "id": 36,
	      "label": "History",
	      "value": "History"
	    },
	    {
	      "id": 27,
	      "label": "Horror",
	      "value": "Horror"
	    },
	    {
	      "id": 10402,
	      "label": "Music",
	      "value": "Music"
	    },
	    {
	      "id": 9648,
	      "label": "Mystery",
	      "value": "Mystery"
	    },
	    {
	      "id": 10749,
	      "label": "Romance",
	      "value": "Romance"
	    },
	    {
	      "id": 878,
	      "label": "Science Fiction",
	      "value": "Science Fiction"
	    },
	    {
	      "id": 10770,
	      "label": "TV Movie",
	      "value": "TV Movie"
	    },
	    {
	      "id": 53,
	      "label": "Thriller",
	      "value": "Thriller"
	    },
	    {
	      "id": 10752,
	      "label": "War",
	      "value": "War"
	    },
	    {
	      "id": 37,
	      "label": "Western",
	      "value": "Western"
	    }
	];

    $scope.selected = $scope.genres[0].id;
    id =  $scope.selected;

    $scope.update = function(filterObject, modelValue, highValue, id) {
		$scope.selected = filterObject;
		id = $scope.selected;
		return id;
		};
		//flick me another fluke button
	$scope.btn = function(){
		if ($scope.selected > 0) {
			 	getMovieGenre();	
			} else {
			 	getMovies();
			}
		$scope.filterCanvas = true;	
		document.getElementById('switch-paddle').style.background = "url(../img/Filters.svg) #DD4F41 no-repeat center center";
		document.getElementById('switch-paddle').style.backgroundSize = "4vh";
	};		
  	function getMovieGenre(filterObject, modelValue, highValue, id){
	    $scope.movie;
	    id = $scope.selected;
	  	  // console.log('getMovies: '+id);
	  	apiService.getRandomTrailerFromGenreId(id).then(function(response) {
	   		id = $scope.selected;
	   		   
	   		$scope.movie = response;
	   			
	   		// $scope.rtng = 'Rating: '+$scope.movie.vote_average;
	 		}, function(err) {
	    	$scope.movie = 'sorry no movies found';
	  	});

  		// preloader fær true því að kallið er hafið í API
		$scope.loader = true;	
		apiService.getMovieGenre(function successCallback(response, modelValue, highValue) {
			$scope.selected.id = id;
			$scope.movie = response.data;
			$scope.str = response.data.release_date;
			$scope.year = $scope.str.substring(0, 4);
			if (response.data.vote_average >= $scope.modelValue && response.data.vote_average <= $scope.highValue && $scope.year >= $scope.modelValueYear && $scope.year <= $scope.highValueYear){
					$scope.title = $scope.movie.original_title;
					$scope.trailer = $sce.trustAsResourceUrl('https://www.youtube.com/embed/'+response.data.trailers.youtube[0].source);
					$scope.rtng = 'Rating: '+$scope.movie.vote_average;
					$scope.premierYear = 'Premiered in: '+response.data.release_date.substring(0, 4);
					}else {
						getMovieGenre();
					}
		// kalli lokið í API og því fær preloder bolean false
			$scope.loader = false;
		}, filterObject, id)
	};

// Hér er aðal functionin sett af stað 
  	function getMovies(filterObject, modelValue, highValue, id){
  		id = 0;
  		// preloader fær true því að kallið er hafið í API
		$scope.loader = true;
	
		// callback frá servise
		allTrailers.getMovies(function successCallback(response, modelValue, highValue, id) {
			id = 0;	
		// kalli lokið í API og því fær preloder bolean false
			$scope.loader = false;
		
		// hér náum við í ártalið sem kemur í formatti yyyy_mm_dd og því þarf að nota substring til að ná í þá tölu sem óskað er eftir sem í þessu tilfelli er yyyy
			$scope.str = response.data.release_date;
			$scope.year = $scope.str.substring(0, 4);
			
		// hér útbum við eittvhað til að halda utanum API JSON kallið
			$scope.movie = response.data;
			
		// hér athugum við hoft að response sé í 1.lagi með trailer og eins berum við saman útkommu úr slæderum við viðeigandi responce gildi.
			if (response.data.trailers.youtube.length > 0 && response.data.trailers.youtube[0].source !== '' && response.data.vote_average >= $scope.modelValue && response.data.vote_average <= $scope.highValue && $scope.year >= $scope.modelValueYear && $scope.year <= $scope.highValueYear)
			 {
					$scope.youTubeSrc = response.data.trailers.youtube[0].source;
					$scope.title = $scope.movie.original_title;
					$scope.rtng = 'Rating: '+$scope.movie.vote_average;
					$scope.premierYear = 'Premiered in: '+response.data.release_date.substring(0, 4);
					$scope.trailer = $sce.trustAsResourceUrl('https://www.youtube.com/embed/'+$scope.youTubeSrc+'');
				
			}
		// annars finnum við bara mynd
			else {
				getMovies();	
			}
			
		}, filterObject, id)
	}
});